// queue.cpp : main project file.

#include "stdafx.h"
#include<iostream>

#include <stdio.h>      /* printf */
#include <stdlib.h>     /* qsort */

#include <algorithm>
#include <ctime>

#include <queue>
#include <vector>
#include <ctime>
//using namespace system;
using namespace std;


///mean filter
class mean {

private: int inMean, outMean, sizeMean;
		 double sum = 0, finalValue;
		 double *queMean;

public:

	//intial function decleare the queue and sets the size
	void intialMean(int length) {
		sizeMean = length;
		queMean = new double[sizeMean];
		inMean = outMean = 0;
		sum = 0.0;

	}//end of intial

	 //insert function to enqueue elements into the queue
	void insertMean(double k) {
		queMean[inMean] = k;
		//	cout << "the inserted median value is: " << queMean[inMean] << endl;
		inMean = (inMean + 1) % sizeMean;
	}//end of insert 


	 //isFull function: if the queue get mean value of queue
	double isFullMean() {
		//	cout << endl;
		if ((inMean + 1) % sizeMean == outMean) {
			//	cout << "the full mean queue is: ";
			//for (size_t i = 0; i < sizeMean - 1; i++)
			//{
				//		cout << queMean[i] << "\t";
			//}

			for (size_t i = 0; i < sizeMean - 1; i++)
			{
				sum = sum + queMean[i];
			}

			//	cout << endl;
			//	cout << endl;
			//	cout << "the sum of the mean filter array is: " << sum << endl << endl;
			finalValue = sum / (sizeMean - 1);
			//	cout << "the final value is: " << finalValue << endl;
			intialMean(sizeMean);
			return finalValue;
		}
		else {
			return 0;
		}

	}//end of isFull

};//end of mean

//create 2 global objects of the mean class one for x and another for y
mean xObjMean, yObjMean;

//a compare funcion helps to sort the array
int cmp(const void *x, const void *y)
{
	double xx = *(double*)x, yy = *(double*)y;
	if (xx < yy) return -1;
	if (xx > yy) return  1;
	return 0;
}


///Median filter
class median {

private: int inMedian, outMedian, medValue = 0, sizeMedian = 6;
		 double *tempArray, *queMedian;
public:

	//intial function decleare the queue and sets the size
	void intialMedian(int length) {
		sizeMedian = length;
		queMedian = new double[sizeMedian];
		inMedian = outMedian = 0;
	}//end of intial

	//insert function to enqueue elements into the queue
	void insertMedian(double k) {
		queMedian[inMedian] = k;
		//	cout << "the inserted element is: " << queMedian[inMedian] << endl;
		inMedian = (inMedian + 1) % sizeMedian;
	}//end of insert 

	//isFull function: if the queue if full call copySort function and give it the queue as argument
	double isFullMedian() {
		if ((inMedian + 1) % sizeMedian == outMedian) {
			//	cout << "the full queue is: ";
			//for (size_t i = 0; i < sizeMedian - 1; i++)
			//{
				//		cout << queMedian[i] << "\t";
			//}
			//	cout << endl;


				//copySort function copies the queue to a temporary queue and sort it to extract the median value
			double v = copySort(queMedian, sizeMedian - 1);
			intialMedian(sizeMedian);
			return v; //v is the median value of the array retrned from copySort function.
		}
		else {
			return 0;
		}
	}//end of isFull

	// when called, it copies the coming queue to a temporary queue and sort it to extract the median value
	//copySort takes the array pointer and the length of the queue as arguments
	double copySort(double *arry, int len) {
		tempArray = new double[len];
		int u = _msize(arry);
		memcpy(tempArray, arry, u);

		//	cout << endl;
		//	cout << "the copied array is: ";
		//for (int n = 0; n < len; n++)
		//{
			//		cout << tempArray[n] << "\t";

		//}
		//	cout << endl;
		//	cout << endl;

			///sort function
			//qsort(Pointer to the first object of the array to be sorted, Number of elements in the array, Size in bytes of each element,Pointer to a function that compares two element)
		qsort(tempArray, len, sizeof(tempArray[0]), cmp);

		//	cout << "the sorted array is: ";
		//for (int n = 0; n < len; n++) {
			//print sorted array
	    //cout << tempArray[n] << "\t";
		//}

		//	cout << endl;

		if (len % 2 == 0)
		{
			medValue = tempArray[len / 2];
		}
		else {
			medValue = tempArray[(len / 2) + 1];
		}
		return medValue;
		//	cout << endl;
	}//end of copySort
};//end of median

// create 2 global objects of the median class one for x and another for y
median xObjMedian, yObjMedian;

//(1)----------D-----------(2)
// .____x___|             .
//  .       |           .
//  R1      |         R2
//    .     y       .
//     .    |     .
//      .   |   .
//       .  | .
//          @
//


//from the drawing:
//(1) is the first reciever and (2) is the second reciever and @ is the pen-transmitter

//D is the distance between the two recivers.
//R1 is the distance from the transmiter to the first reciever
//R2 is the distance from the transmiter to the second reciever

//R1^2 = X^2+ y^2 
//R2^2 = y^2 + (x-D)^2
//so:..
	//x= (D^2 - R2^2 + R1^2)/2D
	//y^2=R1^2-x^2

///calculate values of X & Y:
double* XY(double R1, double R2, int D) {

	//D is the distance between the two recivers.
	//R1 is the distance from the transmiter to the first reciever
	//R2 is the distance from the transmiter to the second reciever

	//l=y^2, m=x^2.
	double l, m;
	static double dimensions[2];

	//d is the D^2
	double d = pow(D, 2);
	//r1 is the R1^2
	double r1 = pow(R1, 2);
	//r2 is the R^2
	double r2 = pow(R2, 2);

	//calculate x
	dimensions[0] = (d - r2 + r1) / (2 * D); 

	//calculate y
	m = pow(dimensions[0], 2);    // m= x^2.
	l = r1 - m;      //y^2 = l 
	dimensions[1] = sqrt(l);	//the value of y
	return dimensions;
}//end of XY


double getXMedian(double x) {
	//insert the 'x' value into the queue:
	if (x < 0) x = 0;
	xObjMedian.insertMedian(x);
	//Check if the queue is full
	return xObjMedian.isFullMedian();
}

double getXMean(double medianX) {
	//cout << "the median value of array X: " << medianX << endl;
	//insert median value to mean array
	xObjMean.insertMean(medianX);
	return xObjMean.isFullMean();
}

double getYMedian(double y) {
	if (y < 0) y = 0;
	//insert the 'y' value into the queue:
	yObjMedian.insertMedian(y);
	return yObjMedian.isFullMedian();
}

double getYMean(double medianY) {
	//	cout << "the median value of array Y: " << medianY << endl;
		//insert median value to mean array
	yObjMean.insertMean(medianY);
	return yObjMean.isFullMean();
}

void initiate() {
	//intial function takes the length of the median queue as an argument.
	//note that: the length of 'x' queue should equel the length of queue 'y'
	//note that: the number of the elements in the queue = lengthe - 1, as this is a circular queue.
	xObjMedian.intialMedian(4);
	yObjMedian.intialMedian(4);

	//intial function takes the length of the mean queue as an argument.
	//note: the intialization of mean should be before calling xy funcion as it plays continiously.
	xObjMean.intialMean(4);
	yObjMean.intialMean(4);
}

int main()
{
	initiate();
	double xMed, yMed, xMean, yMean;
	int r1, r2, d;
	for (int i = 0; i < 20; i++)
	{
		r1 = rand() % 10 + 10;
		r2 = rand() % 10 + 10;
		double* xyValues = XY(r1,r2, 6);
		cout << "R1: " << r1 << endl;
		cout << "R2: " << r2 << endl;
		cout << "X: " << xyValues[0] << endl;
		cout << "Y: " << xyValues[1] << endl;

		xMed = getXMedian(xyValues[0]);
		yMed = getYMedian(xyValues[1]);
		if (xMed != 0)
		{
			cout << "X Median Value:" << xMed << endl ;
			xMean = getXMean(xMed);
			if (xMean != 0)
			{
				cout << "X Mean Value:" << xMean << endl;
			}
		}
		if (yMed != 0)
		{
			cout << "Y Median Value:" << yMed << endl << "-------------" << endl;
			yMean = getYMean(yMed);
			if (yMean != 0)
			{
				cout << "Y Mean Value:" << yMean << endl << "****************" << endl;
			}
		}
	}
	return 0;
}
