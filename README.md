# Smart Pen
Smartpen is a digital note-taking system that enables the user to take notes on regular paper
and see their notes transferred to their computer device where they can share using websites system in real time.

# This represents API to calculate the XY dimensions of the pen in respect to the paper.
* The second step of the project. 

* This API is a test simulation to calculate the XY dimensions based on the received calculated distance transferred from the microcontroller at the [first step](https://bitbucket.org/graduationproject16/smart-pen-embedded/overview) to get the most accurate distance.
 then filter the result to get the most accurate dimensions.
 
* The API include 2 staged filtration:
	* 1) Median Filter
	* 2) Mean Filter 


* Created by Ibrahim Mahdy based on the idea presented in [3D Paint](http://people.ece.cornell.edu/land/courses/ece4760/FinalProjects/s2012/wgm37_gc348/wgm37_gc348/index.html)

To check the project full documentation visit this [link](https://drive.google.com/file/d/1U_OQTZ40pwyhT3x178S9Y90uItywCBWm/view?usp=sharing)
